#ifndef GOODS_CATALOG_H
#define GOODS_CATALOG_H

#include "costants.h"

struct catalog {
	double cost;
	int number;
	char category[MAX_STRING_SIZE];
	char name[MAX_STRING_SIZE];
};

#endif
