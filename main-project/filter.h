#ifndef FILTER_H
#define FILTER_H

#include "goods_catalog.h"

catalog** filter(catalog* array[], int size, bool (*check)(catalog* element), int& result_size);

/*
  <function_name>:
              ,
          true,
    ,

:
    array       -
    size        -
    check       -    .

                   ,
    result_data - ,    - ,



          ,
     (     true)
*/


bool check_catalog_by_category(catalog* element);

/*
  check_book_subscription_by_author:
      - ,

:
    element -   ,


    true,           ,  false
*/


bool check_catalog_by_cost(catalog* element);

/*
  check_book_subscription_by_date:
      - ,           2021-

:
    element -   ,


    true,           2021- ,  false
*/

#endif
