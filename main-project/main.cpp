#include <iostream>
#include <iomanip>

using namespace std;

#include "goods_catalog.h"
#include "file_reader.h"
#include "costants.h"
#include "filter.h"

void output(catalog* product)
{
	/**********   **********/
	cout << "........: ";
	//  
	cout<< "����: " << product->cost << " ����������: ";   
	cout << product->number << ". ";
	   
	cout << product->category << " ";
	cout << product->name << " ";
	  
	
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "Laboratory work #8. GIT\n";
	cout << "Variant #10. Goods Catalog\n";
	cout << "Author: Alisa Shlychkova\n";
	cout << "Group: 16\n";
	catalog* product[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", product, size);
		cout << "*****   *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(product[i]);
		}
		bool (*check_function)(catalog*) = NULL; // check_function -    ,    bool,
														   //        book_subscription*
	
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_catalog_by_category; //       
			cout << "*****    ����������    *****\n\n";
			break;
		case 2:
			check_function = check_catalog_by_cost; //       
			cout << "*****    ���� �� 100  *****\n\n";
			break;
		
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			catalog** filtered = filter(product, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete product[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
