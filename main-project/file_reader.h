#ifndef FILE_READER_H
#define FILE_READER_H

#include "goods_catalog.h"

void read(const char* file_name, catalog* array[], int& size);

#endif